using System;
using System.Collections.Generic;
using Microsoft.AspNet.Mvc;
using PropNet.Models;
using Microsoft.Data.Sqlite;

namespace PropNet.Controllers
{
    [Route("api/[controller]")]
    public class PropertiesController : Controller
    {
        // GET: api/Contacts
        [HttpGet]
        public IEnumerable<Property> Get()
        {
            using (var connection = new SqliteConnection("" +
                    new SqliteConnectionStringBuilder
                    {
                        DataSource = "propmgmt.sqlite"
                    }))
            {
               //  System.Diagnostics.Debug.WriteLine("SomeText");
                connection.Open();

                using (var transaction = connection.BeginTransaction())
                {
                    //  var insertCommand = connection.CreateCommand();
                    //  insertCommand.Transaction = transaction;
                    //  insertCommand.CommandText = "INSERT INTO message ( text ) VALUES ( $text )";
                    //  insertCommand.Parameters.AddWithValue("$text", "Hello, World!");
                    //  insertCommand.ExecuteNonQuery();

                    var selectCommand = connection.CreateCommand();
                    selectCommand.Transaction = transaction;
                    selectCommand.CommandText = "SELECT * FROM properties";

                    List<Property> properties = new List<Property>();
                    using (var reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Property prop = new Property();
                            prop.address = reader.GetString(0);
                            prop.city = reader.GetString(1);
                            prop.state = reader.GetString(2);
                            prop.zip = reader.GetString(3);
                            prop.construction_year = reader.GetInt16(4);
                            prop.purchase_year = reader.GetInt16(5);
                            prop.id = reader.GetInt16(6);
                            properties.Add(prop);
                            Console.WriteLine(prop);
                        }
                    }

                    transaction.Commit();
                    return properties.ToArray();
                //      return new Property[]{
                //                          new Property { id = 1, address = "blarney@contoso.com", city = "Barney Poland"},
                //                          new Property { id = 2, address = "lacy@contoso.com", state = "Lacy Barrera"},
                //                          new Property { id = 3, zip = "lora@microsoft.com", construction_year = 1987 }
                //  };
                }
            }
        }
    }
}