

CREATE TABLE contracts (
    name          TEXT NOT NULL,
    property_id   TEXT NOT NULL,
    from_date     TEXT,
    to_date       TEXT,
    vendor        TEXT,
    website       TEXT,
    amount        INTEGER,
    paid_on       TEXT,
    notes         TEXT,
    id            INTEGER PRIMARY KEY,
    FOREIGN KEY(property_id) REFERENCES properties(id)
);
INSERT INTO contracts VALUES ('1', 'insurance', '2014-07-01', '2015-07-01', 'Home Insurance Corp', '', 76000, '2014-07-15', 'some interesting things', 1);
INSERT INTO contracts VALUES ('2', 'insurance', '2014-07-01', '2015-07-01', 'Insurance Home Corp', '', 76000, '2014-07-15', 'some interesting things', 2);


CREATE TABLE durables (
    name            TEXT NOT NULL,
    make            TEXT NOT NULL,
    model           TEXT NOT NULL,
    year            INTEGER,
    purchase_year   INTEGER,
    property_id     INTEGER,
    id              INTEGER PRIMARY KEY,
    FOREIGN KEY(property_id) REFERENCES properties(id)
);
INSERT INTO durables VALUES ('water heater', 'Whirlpool', '323-XSS', 2009, 2009, 1, 1);
INSERT INTO durables VALUES ('dishwasher', 'GE', 'LLDW100', 2009, 2009, 1, 2);
INSERT INTO durables VALUES ('Air Conditioner', 'Trane', 'dsfa-vkj', 2009, 2009, 2, 3);


CREATE TABLE history (
    property_id   TEXT NOT NULL FOREN,
    item_name     TEXT NOT NULL,
    amount        FLOAT,
    date          TEXT NOT NULL,
    vendor        TEXT,
    notes         TEXT,
    id            INTEGER PRIMARY KEY,
    FOREIGN KEY(property_id) REFERENCES properties(id)
);
INSERT INTO history VALUES ('1', 'rent', 83000.0, '2014-08-01', '', '', 1);
INSERT INTO history VALUES ('1', 'HOA', -12500.0, '2014-08-20', '', '', 2);
INSERT INTO history VALUES ('1', 'house cleaning', -32000.0, '2014-08-01', 'My Carpet Cleaning', 'Some stuff about cleaning', 3);
INSERT INTO history VALUES ('1', 'carpet cleaning', -12500.0, '2014-08-01', 'My Carpet Cleaning', 'Decent job', 4);
INSERT INTO history VALUES ('1', 'rent', 135000.0, '2014-09-17', NULL, NULL, 5);
INSERT INTO history VALUES ('1', 'rent', 135000.0, '2014-07-14', NULL, NULL, 6);
INSERT INTO history VALUES ('1', 'rent', 135000.0, '2014-06-03', NULL, NULL, 7);
INSERT INTO history VALUES ('2', 'rent', 830.0, '2014-08-01', NULL, NULL, 8);


CREATE TABLE leases (
    property_id   INTEGER NOT NULL,
    from_date     TEXT NOT NULL,
    to_date       TEXT,
    deposit       INTEGER,
    id            INTEGER PRIMARY KEY,
    FOREIGN KEY(property_id) REFERENCES properties(id)
);
INSERT INTO leases VALUES (1, '2014-03-01', '2015-03-01', 190000, 1);
INSERT INTO leases VALUES (2, '2014-08-01', '2014-10-01', 0, 2);


CREATE TABLE leases_tenants (
    lease_id    INTEGER,
    tenant_id   INTEGER,
    id          INTEGER PRIMARY KEY,
    FOREIGN KEY(lease_id) REFERENCES leases(id),
    FOREIGN KEY(tenant_id) REFERENCES tenants(id)
);
INSERT INTO leases_tenants VALUES (1, 1, 1);
INSERT INTO leases_tenants VALUES (1, 2, 2);
INSERT INTO leases_tenants VALUES (2, 2, 3);


CREATE TABLE properties (
    address             TEXT NOT NULL,
    city                TEXT NOT NULL,
    state               TEXT NOT NULL,
    zip                 INTEGER NOT NULL,
    construction_year   INTEGER,
    purchase_year       INTEGER,
    id                  INTEGER PRIMARY KEY
);
INSERT INTO properties VALUES ('1313 Mockingbird Ln', 'Anytown', 'NC', 28832, 1984, 2005, 1);
INSERT INTO properties VALUES ('10 Downing St', 'Anytown', 'NC', 28832, 1932, 2000, 2);


CREATE TABLE tenants (
    name      TEXT NOT NULL,
    phone     TEXT,
    email     TEXT,
    id        INTEGER PRIMARY KEY
);
INSERT INTO tenants VALUES ('Maria McDoughnut', '704-555-1212', 'mmcd@email.com', 1);
INSERT INTO tenants VALUES ('Sean McDoughnut', '704-555-1212', 'smcd@email.com', 2);
INSERT INTO tenants VALUES ('Bob Smith', '704-555-1212', 'bob.smith@email.com', 3);
