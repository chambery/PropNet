namespace PropNet.Models
{
    public class Property
    {
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public int construction_year { get; set; }
        public int purchase_year { get; set; }
        public int id { get; set; }
        
        
        public override string ToString()
        {
            return address + ", " + city + ", " +  state + "  " + zip;
        }
    }

}
